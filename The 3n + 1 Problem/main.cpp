#include <iostream>

using namespace std;

int main() {

    int i, j, n, temp, max, min;
    int k = 1;
    temp = 0;
    while (cin >> i >> j) {
        max = i;
        min = j;
        if (j > max) {
            max = j;
            min = i;
        }
        for (int l = min; l <= max; ++l) {
            n = l;
            while (n > 1) {
                if (n % 2 == 0) {
                    n = n / 2;
                }
                else {
                    n = n * 3 + 1;
                }
                k++;
            }
            if (k > temp) {
                temp = k;
            }
            k = 1;
        }

        cout << i << " " << j << " " << temp << endl;
        k = 1;
        temp = 0;
    }

    return 0;
}